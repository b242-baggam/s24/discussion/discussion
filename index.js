// ES6 Updates

// 1. Exponent Operator - **
const firstNum = 8 ** 2;
console.log("Using ** Operator")
console.log(firstNum);

const secondNum = Math.pow(8, 2);
console.log("Using Math.pow();");
console.log(secondNum);

// 2. Template Literals
/*
	- allows us to write strings without using the concatenation operator (+);
*/

let name = "John";

let message = 'Hello ' + name + '! Welcome to programming!';
console.log("Message without template literals: " + message)

// 3. Strings using template literal
// Uses backticks (``)
message = `Hello ${name}! Welcome to programming`;
console.log(`Message with template literals: ${message}`);

// 4. Multi line using template literals
const anotherMessage = 
`${name} attended a math competition
He won it by solving the problem **2 with the solution of $(firstNum);`

console.log(anotherMessage);

const interestRate = 0.1;
const principal = 1000;

console.log(`the interest on your savings account is: ${principal * interestRate}`);

// 5. Array destructuring
/*
	Syntax:
		let/const [variables, variableName, variableName] = array;
*/

const fullName = ['John', 
	'Peters', 'Smith'];

console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you. `);

// 6. Array destructuring example
const [firstName, middleName, lastName] = fullName;
console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you.`);

const nation = ['USA', 'California'];
const [country, state] = nation;
console.log(`Welcome to ${state}, ${country}`);

// 7. Object Destructuring
const person = {
	givenName: 'Jane',
	maidenName: 'Doe',
	familyName: 'Smith'
};

console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again.`);

// 8. Object Destructuring
const {givenName, maidenName, familyName} = person; // for objects, usually flower brackets unlike for arrays, where there are square brackets

console.log(givenName);
console.log(maidenName);
console.log(familyName);

console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again.`)

function getFullName ({givenName, maidenName, familyName}){
	console.log(`${givenName} ${maidenName} ${familyName}`);
}
getFullName(person);

// 9. Arrow function
/*
	Syntax:
		const variableName = () => {
			console.log();
		}
*/

// Hello is the name of the function
const hello = () => {
	console.log('Hello, world!');
}
hello();

const printFullName = (firstName, middleInitial, lastName) => {
	console.log(`${firstName} ${middleInitial}. ${lastName}`);
}

printFullName('John', 'D', 'Smith');

const students = ['John', 'Jane', 'Judy'];

// Arrow functions with loops
console.log('Using traditional function')
// Pre-arrow function
students.forEach(function(student){
	console.log(`${student} is a student.`);
});

// Arrow function
console.log('Using arrow function')
students.forEach((student) => {
	console.log(`${student} is a student.`);
})

// Implicit Return Statement

function add(x, y){
	return x + y;
}

let total = add(1, 2);
console.log(total);

// Arrow function
console.log('Using arrow function');
const addNew = (x, y) => x + y;
let totalNew = addNew(1, 3);
console.log(totalNew);

// Default function Argument Value

const greet = (name = 'User') => {
	return `Good morning, ${name}`;
}

// As we are using a return statement, we need to keep it inside the console statement
console.log(greet());
console.log(greet('Ajay'));